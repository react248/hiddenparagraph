import { Component } from "react";
import "./App.css";

export class App extends Component {
  constructor() {
    super();
    this.state = {
      hidden: false,
    };
  }

  changeState = () => {
    this.setState({ hidden: !this.state.hidden });
  };

  render() {
    return (
      <div>
        <button onClick={this.changeState}>siemka</button>
        <p className={this.state.hidden === true ? "hidden" : null}>A kuku!</p>
      </div>
    );
  }
}
